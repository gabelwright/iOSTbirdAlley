//
//  CTV.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 10/19/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import UIKit
import Firebase

class CTV: UIViewController {
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var navBar: UINavigationBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        var stringUrl = "https://vimeo.com/187357169"
        
        
        let remoteConfig = FIRRemoteConfig.remoteConfig()
        let remoteConfigSettings = FIRRemoteConfigSettings.init(developerModeEnabled: false)
        remoteConfig.configSettings = remoteConfigSettings!
        remoteConfig.setDefaultsFromPlistFileName("RemoteConfigDefaults")
        remoteConfig.fetch(withExpirationDuration: 21600, completionHandler: {(status,error)-> Void in
            remoteConfig.activateFetched()
            stringUrl = remoteConfig.configValue(forKey: "ctv_url").stringValue!
            let url = URL(string: stringUrl)!
            self.webview.loadRequest(URLRequest(url: url))
        })
    }

    
    @IBAction func unwindToMedia(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "unwindToMedia", sender: self)
    }

}









