//
//  AppDelegate.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 8/11/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        FIRApp.configure()
        
        let settings: UIUserNotificationSettings = UIUserNotificationSettings(types:[.alert,.badge,.sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types.rawValue != 0{
            print("Notifications are on")
            checkSubs()
            firstTime()
        }
    }
    func checkSubs(){
        print("Check subs method")
        let settingsDefaults = ["notif_admin","notif_counsel","notif_schoolE","notif_ib","notif_lib"]
        let oldDefaults = ["adminNotif","counselNotif","schoolNotif","ibNotif","libNotif"]
        let topics = ["/topics/administration","/topics/counselor","/topics/schoolEvents","/topics/ibProgram","/topics/library"]
        
        let defaults = UserDefaults.standard
        
        for i in 0..<topics.count{
            print("mgw 1")
            if defaults.bool(forKey: settingsDefaults[i]) != defaults.bool(forKey: oldDefaults[i]){
                print("mgw 2")
                if defaults.bool(forKey: settingsDefaults[i]){
                    print("mgw 3")
                    FIRMessaging.messaging().subscribe(toTopic: topics[i])
                    UserDefaults.standard.set(true, forKey: oldDefaults[i])
                    print("topic subscribed")
                }
                else{
                    print("mgw 4")
                    FIRMessaging.messaging().unsubscribe(fromTopic: topics[i])
                    UserDefaults.standard.set(false, forKey: oldDefaults[i])
                    print("topic unsubscribed")
                }
            }
        }
        print("end of check subs method")
        
        
        
    }
    func firstTime(){
        print("func first time method")
        if !UserDefaults.standard.bool(forKey: "first_time"){
            print("inside loop")
            let array = ["Example Note"]
            print("MGW \(array.count)")
            UserDefaults.standard.set(array, forKey: "userNotes")
            
            UserDefaults.standard.set(true, forKey: "adminNotif")
            UserDefaults.standard.set(true, forKey: "counselNotif")
            UserDefaults.standard.set(true, forKey: "schoolNotif")
            UserDefaults.standard.set(false, forKey: "ibNotif")
            UserDefaults.standard.set(false, forKey: "libraryNotif")
            
            FIRMessaging.messaging().subscribe(toTopic: "/topics/schoolEvents")
            FIRMessaging.messaging().subscribe(toTopic: "/topics/counselor")
            FIRMessaging.messaging().subscribe(toTopic: "/topics/administration")
            UserDefaults.standard.set(true, forKey: "first_time")
        }
    }


}

