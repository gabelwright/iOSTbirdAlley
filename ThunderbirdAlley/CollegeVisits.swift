//
//  CollegeVisits.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 10/19/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import UIKit
import Firebase

class CollegeVisits: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var collegeList = [collegeVisitObject]()

    override func viewDidLoad() {
        super.viewDidLoad()

        //sets date 24 hours later to prevent students from signing up within 24 hours of visit
        let todaysDate = NSDate().addingTimeInterval(24*60*60)
        let dataRef = FIRDatabase.database().reference()
        dataRef.child("collegeList").observe(FIRDataEventType.value, with: {(snapshot) in
            self.collegeList.removeAll()
            for ds in snapshot.children.allObjects{
                let dateMilli = (ds as AnyObject).childSnapshot(forPath: "dateTime").value as! Double
                let dateVisit = dateMilli/1000
                let visitDate = NSDate(timeIntervalSince1970: dateVisit) as Date
                
                if todaysDate.earlierDate(visitDate) == todaysDate as Date{
                    let college = (ds as AnyObject).childSnapshot(forPath: "college").value as! String
                    self.collegeList.append(collegeVisitObject(college: college, date: visitDate as NSDate!))
                }
            }
        
        self.tableView.reloadData()
        })
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let info = UserDefaults.standard
        if ((info.string(forKey: "user_name")) ?? "").isEmpty  || ((info.string(forKey: "user_id")) ?? "").isEmpty || ((info.string(forKey: "user_grade_level")) ?? "").isEmpty{
            let alert = UIAlertController(title: "Missing Information", message: "Please enter your Name, ID number, and Grade Level in the settings page", preferredStyle: UIAlertControllerStyle.alert)
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    } else {
                        let alert2 = UIAlertController(title: "Navigation failed", message: "Could not automatically navigate to settings. Please go to the app settings and update your info.", preferredStyle: UIAlertControllerStyle.alert)
                        let button2 = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                        alert2.addAction(button2)
                        self.present(alert2, animated: true, completion: nil)
                    }
                }
            }
            alert.addAction(settingsAction)
            let okButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
            alert.addAction(okButton)
            present(alert, animated: true, completion: nil)
        }else{
            let name:String = info.string(forKey: "user_name")!
            let id:String = info.string(forKey: "user_id")!
            var grade:String = "Grade Unknown"
            switch (info.integer(forKey: "user_grade_level")){
                case 0:
                    grade = "9th Grade"
                    break
                case 1:
                    grade = "10th Grade"
                    break
                case 2:
                    grade = "11th Grade"
                    break
                case 3:
                    grade = "12th Grade"
                    break
                default:
                    grade = "Parent/Teacher"
                    break
            }
            
            let alert = UIAlertController(title: "Confirm your Information", message: "Please confirm your name, ID number, and grade are correct: \n\(name)\n\(id)\n\(grade)", preferredStyle: UIAlertControllerStyle.alert)
            
            let okButton = UIAlertAction(title: "Sign Up", style: UIAlertActionStyle.default, handler: {action in
                self.signUp(date: self.collegeList[indexPath.row].date as Date, name: name, id: id, grade: grade)
            
            })
            let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
            alert.addAction(okButton)
            alert.addAction(cancelButton)
            present(alert, animated: true, completion: nil)
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "visitCell",for: indexPath) as! CollegeVisitsTableViewCell
        
        if(collegeList.count == 0){
            cell.collegeNameLabel.text = "No upcoming college visits :("
            cell.collegeDateTime.text = ""
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.isUserInteractionEnabled = false
        }
        else{
            let monthF = DateFormatter()
            monthF.dateFormat = "MMM dd h:mm a"
            
            cell.collegeNameLabel.text = collegeList[indexPath.row].college
            let month = monthF.string(from: collegeList[indexPath.row].date as Date)
            cell.collegeDateTime.text = month
            
            cell.selectionStyle = UITableViewCellSelectionStyle.default
            cell.isUserInteractionEnabled = true
        }
        return cell
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if(collegeList.count == 0){
            return 1
        }else{
            return collegeList.count
        }
        
        
    }
    
    public func signUp(date:Date, name:String, id:String, grade:String){
        let dataRef = FIRDatabase.database().reference()
        let timeStamp = Int64(date.timeIntervalSince1970) * 1000
        
        let heading = "collegeVisits/\(timeStamp)/students"
        print(heading)
        let key = dataRef.child(heading).childByAutoId().key
        print(key)
        let post = ["name": name,
                    "id": id,
                    "grade":grade]
        dataRef.child(heading).child(key).setValue(post)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func unwindToCC(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "unwindToCC", sender: self)
    }
}

struct collegeVisitObject {
    let college : String!
    let date : NSDate!
}

