//
//  NotesTabPage.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 8/13/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import UIKit

class NotesTabPage: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noteCell: UITableViewCell!
    
    var userNotes = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        

        }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
        print("View did appear")
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == UITableViewCellEditingStyle.delete){
            userNotes = UserDefaults.standard.array(forKey: "userNotes") as! [String]
            userNotes.remove(at: indexPath.row)
            UserDefaults.standard.set(userNotes, forKey: "userNotes")
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        
        return (UserDefaults.standard.array(forKey: "userNotes")?.count)!
    }
    
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "noteCell")
        let userNotes = UserDefaults.standard.array(forKey: "userNotes") as! [String]
        cell.textLabel?.text = userNotes[indexPath.row]
        //cell.textLabel?.font = UIFont(name: "Chalkduster", size: 20)
        cell.textLabel?.textAlignment = .justified
        
        cell.contentView.backgroundColor = UIColor(red: 0.4, green: 0.6, blue: 0, alpha: 0.5)
        
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        
        
        return cell
    }
    
    @IBAction func unwindToSE(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "unwindToSE", sender: self)
    }

}
