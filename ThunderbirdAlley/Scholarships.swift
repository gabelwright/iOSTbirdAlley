//
//  Scholarships.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 10/21/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import UIKit

class Scholarships: UIViewController {
    @IBOutlet weak var webview: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: "https://web35.secure-secure.co.uk/acronymcreations.com/tbirdalley/scholarships.html")!
        
        webview.loadRequest(URLRequest(url: url))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToCC(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "unwindToCC", sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
