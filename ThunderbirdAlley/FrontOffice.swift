//
//  FrontOffice.swift
//  
//
//  Created by M.Wright on 10/18/16.
//
//

import UIKit
import Firebase

class FrontOffice: UIViewController {
    @IBOutlet weak var openMenu: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        openMenu.target = self.revealViewController()
        openMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        self.revealViewController().rearViewRevealWidth = 200
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

        FIRAuth.auth()?.signInAnonymously() { (user, error) in
            if error == nil{
                print("user signed in as \(user)")
            }else{
                print("user sign in error: \(error.debugDescription)")
            }
        }
        
        let token = FIRInstanceID.instanceID().token()
        if token != nil{
            print("Fire Token: \(token)")
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToFrontOffice(segue: UIStoryboardSegue) {}

    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            
        }
        else{
            print("InstanceID token failed")
        }
    }
    
    
    
    


}
