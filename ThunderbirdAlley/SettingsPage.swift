//
//  SettingsPage.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 8/12/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import Foundation
import Firebase

class SettingsPage:UIViewController{
    @IBOutlet weak var openMenu: UIBarButtonItem!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var classSegment: UISegmentedControl!
    
    @IBOutlet weak var adminSegment: UISegmentedControl!
    
    @IBOutlet weak var ibSegemnt: UISegmentedControl!
    @IBOutlet weak var schoolESegment: UISegmentedControl!
    @IBOutlet weak var counselSegment: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        openMenu.target = self.revealViewController()
        openMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        self.revealViewController().rearViewRevealWidth = 185
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
        if let versionName = nsObject{
            versionLabel.text = "Version: \(versionName)"
        }
        let segmentX:Float = 0.7
        let segmentY:Float = segmentX
        adminSegment.transform = CGAffineTransform(scaleX: CGFloat(segmentX), y: CGFloat(segmentY))
        counselSegment.transform = CGAffineTransform(scaleX: CGFloat(segmentX), y: CGFloat(segmentY))
        schoolESegment.transform = CGAffineTransform(scaleX: CGFloat(segmentX), y: CGFloat(segmentY))
        ibSegemnt.transform = CGAffineTransform(scaleX: CGFloat(segmentX), y: CGFloat(segmentY))
        
        classSegment.selectedSegmentIndex = UserDefaults.standard.integer(forKey: "classification")
        adminSegment.selectedSegmentIndex = UserDefaults.standard.integer(forKey: "adminNotif")
        counselSegment.selectedSegmentIndex = UserDefaults.standard.integer(forKey: "counselNotif")
        schoolESegment.selectedSegmentIndex = UserDefaults.standard.integer(forKey: "schoolNotif")
        ibSegemnt.selectedSegmentIndex = UserDefaults.standard.integer(forKey: "ibNotif")
        
    }
    
    
    
    @IBAction func classSegmentAction(_ sender: AnyObject) {
        UserDefaults.standard.set(classSegment.selectedSegmentIndex, forKey: "classification")
        
    }
    @IBAction func adminSegmentAction(_ sender: AnyObject) {
        UserDefaults.standard.set(adminSegment.selectedSegmentIndex, forKey: "adminNotif")
        if adminSegment.selectedSegmentIndex == 0{
            FIRMessaging.messaging().subscribe(toTopic: "/topics/administration")
        }else{
            FIRMessaging.messaging().unsubscribe(fromTopic: "/topics/administration")
        }
    }
    @IBAction func counselSegmentAction(_ sender: AnyObject) {
        UserDefaults.standard.set(counselSegment.selectedSegmentIndex, forKey: "counselNotif")
        if counselSegment.selectedSegmentIndex == 0{
            FIRMessaging.messaging().subscribe(toTopic: "/topics/counselor")
        }else{
            FIRMessaging.messaging().unsubscribe(fromTopic: "/topics/counselor")
        }
    }
    @IBAction func schoolSegmentAction(_ sender: AnyObject) {
        UserDefaults.standard.set(schoolESegment.selectedSegmentIndex, forKey: "schoolNotif")
        if schoolESegment.selectedSegmentIndex == 0{
            FIRMessaging.messaging().subscribe(toTopic: "/topics/schoolEvents")
        }else{
            FIRMessaging.messaging().unsubscribe(fromTopic: "/topics/schoolEvents")
        }
    }
    @IBAction func ibSegmentAction(_ sender: AnyObject) {
        UserDefaults.standard.set(ibSegemnt.selectedSegmentIndex, forKey: "ibNotif")
        if ibSegemnt.selectedSegmentIndex == 0{
            FIRMessaging.messaging().subscribe(toTopic: "/topics/ibProgram")
        }else{
            FIRMessaging.messaging().unsubscribe(fromTopic: "/topics/ibProgram")
        }
    }
    
    
    

}














