//
//  VotingPage.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 8/25/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import UIKit
import Firebase

class VotingPage: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
        
    var votingArray = [String]()
    var resultsArray:[Float] = []
    var totalVoted:Float = 0.0
    var votingAlowed:Bool = true
    var pollNumber = 0
    var question:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let remoteConfig = FIRRemoteConfig.remoteConfig()
        
        //set developer mode
        let remoteConfigSettings = FIRRemoteConfigSettings.init(developerModeEnabled: false)
        remoteConfig.configSettings = remoteConfigSettings!
        remoteConfig.setDefaultsFromPlistFileName("RemoteConfigDefaults")
        remoteConfig.fetch(withExpirationDuration: 21600, completionHandler: { (status,error)-> Void in
            print("Fetch Sucessful")
            remoteConfig.activateFetched()
            self.pollNumber = remoteConfig.configValue(forKey: "voting_count").numberValue as! Int
            self.question = remoteConfig.configValue(forKey: "voting_question").stringValue!
            self.questionLabel.text = self.question
            self.votingArray = (remoteConfig.configValue(forKey: "voting_options").stringValue?.components(separatedBy: ":"))!
            
            if UserDefaults.standard.integer(forKey: "votingCount") < self.pollNumber{
                
                self.allowVoting()
            }else{
                
                self.showResults()
            }
            
        })
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dataRef = FIRDatabase.database().reference()
        
        dataRef.child("currentVote").runTransactionBlock({(currentData:FIRMutableData)->FIRTransactionResult in
            var voteTally = currentData.value as? [String:AnyObject]
            var count = voteTally?[self.votingArray[indexPath.row]] as? Int ?? 0
            count += 1
            voteTally?[self.votingArray[indexPath.row]] = count as AnyObject? 
            currentData.value = voteTally
            
            return FIRTransactionResult.success(withValue: currentData)
            
        }, andCompletionBlock: {(error,someBool,datashot)->Void in
            if error == nil{
                UserDefaults.standard.set(self.pollNumber, forKey: "votingCount")
                let example = UserDefaults.standard.integer(forKey: "votingCount")
                print("MGW default: \(example)+ \(self.pollNumber)")
                self.showResults()
            }
        
        
        })
        
        
        
    }
    
    
    func allowVoting(){
        self.votingAlowed = true
        self.tableView.allowsSelection = true
        self.questionLabel.text = question
        self.tableView.reloadData()
    }
    
    func showResults(){
        self.votingAlowed = false
        self.tableView.allowsSelection = false
        //votingArray.removeAll()
        
        //questionLabel.text = question
        print("MGW start")
        let dataRef = FIRDatabase.database().reference()
        dataRef.child("currentVote").observe(FIRDataEventType.value, with: {(snapshot) in
            self.totalVoted = 0
            self.resultsArray.removeAll()
            let voteDic = snapshot.value as! [String:AnyObject]
            for value in self.votingArray{
                var voteCount:Float = 0
                let voteCountOptional = voteDic[value]
                if voteCountOptional != nil{
                    voteCount = voteCountOptional as! Float
                }
                print(voteCount)
                self.resultsArray.append(voteCount)
                self.totalVoted += voteCount
            }
            self.tableView.reloadData()
            
        
        })
        
    
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return votingArray.count
    }
    
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "votingCell", for: indexPath) as! VotingTableViewCell
        
        cell.labelText.text = votingArray[indexPath.row]
        
        if votingAlowed{
            cell.percent.text = ""
            cell.progressbar.isHidden = true
        }
        else if !resultsArray.isEmpty{
            let percent = (resultsArray[indexPath.row]/totalVoted) * 100
            cell.percent.text = NSString(format: "%.0f%%", percent) as String
            cell.progressbar.isHidden = false
            cell.progressbar.progress = Float(resultsArray[indexPath.row]/totalVoted)
        }
        
        
        return cell
    }
    
    @IBAction func unwindToSe(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "unwindToSE", sender: self)
    }

    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
