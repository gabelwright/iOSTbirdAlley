//
//  NotifTableViewCell.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 8/17/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import UIKit

class NotifTableViewCell: UITableViewCell {
    @IBOutlet weak var notifTitle: UILabel!
    @IBOutlet weak var notifMessage: UILabel!

    @IBOutlet weak var notifImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
