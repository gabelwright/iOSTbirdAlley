//
//  AddNoteTabPage.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 8/13/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import UIKit

class AddNoteTabPage: UIViewController {
    @IBOutlet weak var textfield: UITextView!
    
    @IBOutlet weak var button: UIButton!
    
    @IBAction func saveButton(_ sender: AnyObject) {
        saveButtonMethod()
    }
    @IBAction func primaryActionTriggered(_ sender: AnyObject) {
        saveButtonMethod()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        button.layer.cornerRadius = 10
        textfield.layer.cornerRadius = 10
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    func saveButtonMethod(){
        var userNotes = UserDefaults.standard.array(forKey: "userNotes")
        userNotes?.append(textfield.text!)
        UserDefaults.standard.set(userNotes, forKey: "userNotes")
        
        textfield.text = ""
        self.textfield.endEditing(true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
