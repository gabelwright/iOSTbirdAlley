//
//  PolicyPage.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 8/12/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import Foundation

class PolicyPage:UIViewController{
    @IBOutlet weak var webview: UIWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: "https://web35.secure-secure.co.uk/acronymcreations.com/policy.html")!
        
        webview.loadRequest(URLRequest(url: url))
    }
    
    @IBAction func backButton(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "unwindToFrontOffice", sender: self)
    }
}
