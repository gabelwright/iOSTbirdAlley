//
//  SpiritPhotos.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 10/25/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import UIKit
import Firebase

class SpiritPhotos: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var uploadButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        textLabel.text = "Show your school pride by sharing your amazing pictures from all of your favorite school events. Anything you contribute may show up in the yearbook, newspaper, or even within this app, so go ahead, give it a shot!"
        
//        button.backgroundColor = UIColor.clear
//        button.layer.cornerRadius = 5
//        button.layer.borderWidth = 1
//        button.layer.borderColor = UIColor.black.cgColor
        uploadButton.layer.cornerRadius = 10
        progressBar.isHidden = true

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            let info = UserDefaults.standard
            let username:String = (info.string(forKey: "user_name")?.replacingOccurrences(of: " ", with: ""))!
            let id:String = info.string(forKey: "user_id")!
            
            let dateformatter1 = DateFormatter()
            dateformatter1.dateStyle = DateFormatter.Style.medium
            dateformatter1.timeStyle = DateFormatter.Style.medium
            let fileName = dateformatter1.string(from: NSDate() as Date)
                .replacingOccurrences(of: " ", with: "")
                .replacingOccurrences(of: ",", with: "")
                .replacingOccurrences(of: ":", with: "")
            
            let storage = FIRStorage.storage().reference(withPath: "SpiritPhotos/\(username)_\(id)/\(fileName).jpeg")
            
            let imageData = UIImageJPEGRepresentation(image, 1.0)
            let metaData = FIRStorageMetadata()
            metaData.contentType = "image/jpeg"
            
            print("MGW about to start")
            progressBar.isHidden = false
            
            //let uplaodTask = storage.put(imageData!,metaData)
            let uploadTask = storage.put(imageData!, metadata: metaData) {(meta, error) in
                if error == nil{
                    let compAlert = UIAlertController(title: "Success", message: "Your photo has been uploaded, thanks for sharing!", preferredStyle: UIAlertControllerStyle.alert)
                    let doneButton = UIAlertAction(title: "Done", style: .cancel){(_)->Void in
                        self.progressBar.isHidden = true
                    }

                    compAlert.addAction(doneButton)
                    self.present(compAlert, animated: true, completion: nil)
                }else{
                    let compAlert = UIAlertController(title: "Failed :(", message: "Your photo failed to upload. Please try again later.", preferredStyle: UIAlertControllerStyle.alert)
                    let doneButton = UIAlertAction(title: "Done", style: .cancel){(_)->Void in
                        self.progressBar.isHidden = true
                    }
                    compAlert.addAction(doneButton)
                    self.present(compAlert, animated: true, completion: nil)
                }
            
            
            }
            
            uploadTask.observe(.progress, handler: {(snapshot) in
                let progress = snapshot.progress
                self.progressBar.progress = Float((progress?.fractionCompleted)!)
            
            })
            
        }else{
            print("MGW Error")
        }
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func importPhoto(_ sender: AnyObject) {
        let info = UserDefaults.standard
        print("MGW \(info.string(forKey: "user_name"))")
        if ((info.string(forKey: "user_name")) ?? "").isEmpty || ((info.string(forKey: "user_id")) ?? "").isEmpty{
            let alert = UIAlertController(title: "Missing Information", message: "Please enter your Name, ID number, and Grade Level in the settings page", preferredStyle: UIAlertControllerStyle.alert)
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Settings opened: \(success)") // Prints true
                        })
                    } else {
                        let alert2 = UIAlertController(title: "Navigation failed", message: "Could not automatically navigate to settings. Please go to the app settings and update your info.", preferredStyle: UIAlertControllerStyle.alert)
                        let button2 = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil)
                        alert2.addAction(button2)
                        self.present(alert2, animated: true, completion: nil)
                    }
                }
            }
            alert.addAction(settingsAction)
            let okButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
            alert.addAction(okButton)
            present(alert, animated: true, completion: nil)
        }else{
            let name:String = info.string(forKey: "user_name")!
            let id:String = info.string(forKey: "user_id")!
            
            let alert = UIAlertController(title: "Confirm your Information", message: "Please confirm your name and ID number are correct: \n\(name)\n\(id)", preferredStyle: UIAlertControllerStyle.alert)
            
            let okButton = UIAlertAction(title: "Correct", style: UIAlertActionStyle.default, handler: {action in
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            })
            let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
            alert.addAction(okButton)
            alert.addAction(cancelButton)
            present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func unwindToMedia(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "unwindToMedia", sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
