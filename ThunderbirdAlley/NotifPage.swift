//
//  NotifPage.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 8/12/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import Foundation
import Firebase

class NotifPage:UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var tableView: UITableView!
    
    var notifList = [NotifObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dataRef = FIRDatabase.database().reference()
        _ = dataRef.child("notifications").observe(FIRDataEventType.value, with: {(snapshot) in
            self.notifList.removeAll()
            for ds in snapshot.children.allObjects {
                let title = (ds as AnyObject).childSnapshot(forPath: "title").value as! String
                let message = (ds as AnyObject).childSnapshot(forPath: "message").value as! String
                let topic = (ds as AnyObject).childSnapshot(forPath: "topic").value as! String
                
                self.notifList.append(NotifObject(title: title, message: message, topic: topic))
            }
            self.tableView.reloadData()
            
        })
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        //tableView.dequeueReusableCell(withIdentifier: "MealTableViewCell", for: indexPath) as! MealTableViewCell
        let cell = tableView.dequeueReusableCell(withIdentifier: "notifCell", for: indexPath) as! NotifTableViewCell
        cell.notifTitle.text = notifList[indexPath.row].title
        cell.notifMessage.text = notifList[indexPath.row].message
        //cell.notifMessage.numberOfLines = 3
        //cell.notifMessage.sizeToFit()
        cell.notifImage.image = notifList[indexPath.row].icon
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        //let cell:NotifTableViewCell = tableView.cellForRow(at: indexPath) as! NotifTableViewCell
        
        
        let alert = UIAlertController(title: notifList[indexPath.row].title, message: notifList[indexPath.row].message, preferredStyle: UIAlertControllerStyle.alert)
        let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(okButton)
        let iconView = UIImageView(frame: CGRect(x: -5, y: 5, width: 40, height: 40))
        iconView.image = notifList[indexPath.row].icon
        alert.view.addSubview(iconView)
        present(alert, animated: true, completion: nil)
    }
    
    //public func tabl
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return notifList.count
    }
    
    @IBAction func unwindToFrontOffice(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "unwindToFrontOffice", sender: self)
    }
    
}

struct NotifObject {
    let title:String
    let message:String
    let icon:UIImage
    
    init(title:String,message:String,topic:String) {
        self.title = title
        self.message = message
        
        switch topic {
        case "administration":
            self.icon = UIImage(named: "notif_a")!
            break
        case "counselor":
            self.icon = UIImage(named: "notif_c")!
            break
        case "schoolEvents":
            self.icon = UIImage(named: "notif_e")!
            break
        case "ibProgram":
            self.icon = UIImage(named: "notif_ib")!
            break
        default:
            self.icon = UIImage(named: "notif_a")!
        }
    }
}












