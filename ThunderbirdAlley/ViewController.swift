//
//  ViewController.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 8/11/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var openMenu: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        openMenu.target = self.revealViewController()
        openMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

