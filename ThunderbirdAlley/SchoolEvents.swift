//
//  SchoolEvents.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 10/18/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import UIKit

class SchoolEvents: UIViewController {
    @IBOutlet weak var openMenu: UIBarButtonItem!
    @IBAction func unwindToSE(segue: UIStoryboardSegue) {}

    override func viewDidLoad() {
        super.viewDidLoad()
        openMenu.target = self.revealViewController()
        openMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        
        self.revealViewController().rearViewRevealWidth = 200
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
