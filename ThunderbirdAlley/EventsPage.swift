//
//  EventsPage.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 8/12/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase

class EventsPage : UIViewController, UITableViewDelegate, UITableViewDataSource{
    var events = [eventObject]()
    
    @IBOutlet weak var eventTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        let todaysDate = NSDate()
        
        let dataRef = FIRDatabase.database().reference()
        _ = dataRef.child("events").observe(FIRDataEventType.value, with: {(snapshot) in
            self.events.removeAll()
            for ds in snapshot.children.allObjects{
                let dateMilli = (ds as AnyObject).childSnapshot(forPath: "dateTime").value as! Double
                let dateEventTime = dateMilli/1000
                let eventDate = NSDate(timeIntervalSince1970: dateEventTime) as Date
                
                if todaysDate.earlierDate(eventDate) == todaysDate as Date{
                    let title = (ds as AnyObject).childSnapshot(forPath: "name").value as! String
                    let location = (ds as AnyObject).childSnapshot(forPath: "location").value as! String
                    
                    self.events.append(eventObject(title: title, location: location, date: eventDate as NSDate!))
                }
            }
            
            self.eventTableView.reloadData()
            
        })
        
        
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if(events.count == 0){
            return 1
        }else{
            return events.count
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as! EventsTableViewCell
        
        if(events.count == 0){
            cell.titleLabel.text = "No upcoming Events  :("
            cell.locationLabel.text = ""
            cell.dateLabel.text = ""
            cell.monthLabel.text = ""
            cell.timeLabel.text = ""
        }
        else{
            let monthF = DateFormatter()
            monthF.dateFormat = "MMM"
            let dateF = DateFormatter()
            dateF.dateFormat = "dd"
            let timeF = DateFormatter()
            timeF.dateFormat = "h:mm a"
            
            cell.titleLabel.text = events[indexPath.row].title
            cell.locationLabel.text = events[indexPath.row].location
            cell.dateLabel.text = dateF.string(from: events[indexPath.row].date as Date)
            cell.monthLabel.text = monthF.string(from: events[indexPath.row].date as Date)
            cell.timeLabel.text = timeF.string(from: events[indexPath.row].date as Date)
        }
        return cell
        
    }
    
    func postObject(){
        let title = "This is a title"
        let message = "This is a message"
        
        let postObject : [String:String] = ["title":title,"message":message]
        
        let ref = FIRDatabase.database().reference()
        ref.child("NewPost").childByAutoId().setValue(postObject)
    }
    @IBAction func unwindToSE(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "unwindToSE", sender: self)
    }
}

struct eventObject {
    let title : String!
    let location : String!
    let date : NSDate!
}
