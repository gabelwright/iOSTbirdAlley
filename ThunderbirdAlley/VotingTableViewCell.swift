//
//  VotingTableViewCell.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 10/28/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import UIKit

class VotingTableViewCell: UITableViewCell {

    
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var percent: UILabel!
    @IBOutlet weak var progressbar: UIProgressView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
