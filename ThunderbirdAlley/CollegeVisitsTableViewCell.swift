//
//  CollegeVisitsTableViewCell.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 10/19/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import UIKit

class CollegeVisitsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collegeNameLabel: UILabel!
    @IBOutlet weak var collegeDateTime: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

