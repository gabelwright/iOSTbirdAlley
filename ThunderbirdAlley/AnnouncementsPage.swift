//
//  AnnouncementsPage.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 8/12/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import Foundation
import Firebase

class AnnouncementsPage:UIViewController{
    @IBOutlet weak var announceWebView: UIWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let url = URL(string: "https://web35.secure-secure.co.uk/acronymcreations.com/announcements.html")!
        
        announceWebView.loadRequest(URLRequest(url: url))
        
    }
    
        
    
    
    @IBAction func unwindToFrontOffice(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "unwindToFrontOffice", sender: self)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
