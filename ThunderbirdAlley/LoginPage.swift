//
//  LoginPage.swift
//  ThunderbirdAlley
//
//  Created by M.Wright on 8/25/16.
//  Copyright © 2016 AcronymCreations. All rights reserved.
//

import UIKit
import Firebase

class LoginPage: UIViewController {
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var openMenu: UIBarButtonItem!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        openMenu.target = self.revealViewController()
        openMenu.action = #selector(SWRevealViewController.revealToggle(_:))
        self.revealViewController().rearViewRevealWidth = 185
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

        FIRAuth.auth()?.addStateDidChangeListener { auth, user in
            if user != nil && (user?.isEmailVerified)!{
                //TODO code to segue to proper page
                
            }else if(user != nil && !(user?.isEmailVerified)!){
                self.passwordTextField.text = ""
                let alert = UIAlertController(title: "Email not Verified", message: "You must verify your email address before continuing. Would you like to resend the verification email? If you have already verified your email, you may need to sign out and then sign back in.", preferredStyle: UIAlertControllerStyle.alert)
                let yesButton = UIAlertAction(title: "Yes", style: UIAlertActionStyle.cancel){(ACTION) in
                    user?.sendEmailVerification(completion: nil)
                    print("email sent")
                }
                let noButton = UIAlertAction(title: "No", style: UIAlertActionStyle.destructive, handler: nil)
                alert.addAction(yesButton)
                alert.addAction(noButton)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

    @IBAction func signInButton(_ sender: AnyObject) {
        errorLabel.text = ""
        FIRAuth.auth()?.signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) {(user,error) in
            if error != nil{
                self.errorLabel.text = "Invalid email or password"
            }
        }
    }
    
    @IBAction func createButton(_ sender: AnyObject) {
        errorLabel.text = ""
        if !(emailTextField.text?.contains("@episd.org"))!{
            errorLabel.text = "An @episd.org email address must be used"
        }
        else if (passwordTextField.text?.characters.count)! < 6{
            errorLabel.text = "Password must be at least 6 characters long"
        }else{
            FIRAuth.auth()?.createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
                if error == nil{
                    user?.sendEmailVerification(completion: nil)
                    let alert = UIAlertController(title: "Account Created", message: "A confirmation email has been sent to your address. Please confirm your account before logging in.", preferredStyle: UIAlertControllerStyle.alert)
                    let okButton = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                    alert.addAction(okButton)
                    self.present(alert, animated: true, completion: nil)
                    try! FIRAuth.auth()?.signOut()
                    
                }
                else{
                    self.errorLabel.text = "An error occured, please double check your email and password."
                }
            }
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
